package activity;

public class Main {
    public static void main(String[] args) {
        /* Non-empty example */
        System.out.println("Case 1");
        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact("Imman", "09876543210", "Cavite");
        Contact contact2 = new Contact("Eula", "123", "Mondstadt");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        phonebook.viewContacts();

        /* Empty example */
        System.out.println("Case 2");
        Phonebook emptyPhoneBook = new Phonebook();

        emptyPhoneBook.viewContacts();

        /* Phonebook has Contacts but all contacts are empty */
        System.out.println("Case 3");
        Phonebook phonebookWithEmptyContacts = new Phonebook();
        Contact contact3 = new Contact();
        Contact contact4 = new Contact();

        phonebookWithEmptyContacts.setContacts(contact3);
        phonebookWithEmptyContacts.setContacts(contact4);

        phonebookWithEmptyContacts.viewContacts();

        /* Phonebook has contacts but some contacts are empty*/
        System.out.println("Case 4");
        Phonebook phonebookWithSomeEmptyContacts = new Phonebook();
        Contact contact5 = new Contact("Corvo", "1234567890", "Karnaca");
        Contact contact6 = new Contact();

        phonebookWithSomeEmptyContacts.setContacts(contact5);
        phonebookWithSomeEmptyContacts.setContacts(contact6);

        phonebookWithSomeEmptyContacts.viewContacts();
    }
}
